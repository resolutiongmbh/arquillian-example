# Arquillian Example Project

This is an example project to demonstrate how to run Arquillian-based integration-tests
on existing instances of Atlassian Server and Datacenter-instances.

### Talk at AtlasCamp 2019 
This ist the example code for the Talk _Integration Testing on Steroids: Run Your Tests on the Real Things_ 
at AtlasCamp 2019: https://www.atlassian.com/atlascamp/schedule?sessionid=567505

(Link to the video follows as soon as it's published)

## Arquillian Containers for Atlassian Applications
This is based on Adaptavist's Arquillian Containers for Atlassian Applications, see 
https://bitbucket.org/Adaptavist/atlassian-arquillian-containers/src

## UPM-Maven-Plugin
The UPM-Maven-Plugin for installing your app during build can be found here: 
https://bitbucket.org/resolutiongmbh/upm-maven-plugin

## Quick start
Edit some properties in the pom.xml to match your instances or override them using `-Dupm.host=https://...` 
when running the command below:

    <properties>
        ...
        <testhost>your.instance.example.com</testhost>
        <upm.user>your_instances_admin_user</upm.user>
        <upm.password>your_instances_admin_password</upm.password>
    </properties>

Run it with `atlas-mvn verify`

This should build the app, install it in your instance and run the test.

## Run tests from Intellij IDEA
Intellij IDEA provides support for Arquillian. In the configuration, choose manual container configuration and specifiy
the identifier from your `arquillian.xml` 
![picture](install_run_from_Intellij.gif)

## Feedback
Feedback is appreciated, please send us an email to arquillian@resolution.de
