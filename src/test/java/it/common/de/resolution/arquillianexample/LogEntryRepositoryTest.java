package it.common.de.resolution.arquillianexample;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import de.resolution.arquillianexample.LogEntry;
import de.resolution.arquillianexample.LogEntryRepository;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.Assignable;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

@RunWith(Arquillian.class) // This is what makes it an Arquillian-test
public class LogEntryRepositoryTest {

    /*
     * Inject the class under test. This dependency injection is what
     * differentiates such a test from a unit-test. No need to mock anymore :-)
     */
    @Inject
    @ComponentImport
    private LogEntryRepository logEntryRepository;

    private static final Logger logger = LoggerFactory.getLogger(LogEntryRepositoryTest.class);
    private final Properties envProp;

    /* Loads the properties generated during deployment
     */
    public LogEntryRepositoryTest() {
        envProp = new Properties();
        try {
            envProp.load(LogEntryRepositoryTest.class.getResourceAsStream("/env.properties"));
        } catch (IOException e) {
            throw new RuntimeException("Loading env.properties failed",e);
        }
    }


    /*
     * This is a simple test for the repisitory- create some data, fetch it again and
     * check that it's there.
     */
    @Test
    public void runTest() {

        // For demonstration, we log the environment variables.
        // In a real test, do something useful with it :-)
        for(String key : envProp.stringPropertyNames()) {
            logger.warn("## {} : {} ", key, envProp.getProperty(key));
        }

        long start = System.currentTimeMillis();

        LogEntry e0 = LogEntry.builder().add("msg","Message0").add("ip","10.10.10.0").build();
        logEntryRepository.add(e0);
        long after0 = System.currentTimeMillis();

        LogEntry e1 = LogEntry.builder().add("msg","Message").add("ip","10.10.10.10").build();
        LogEntry e2 = LogEntry.builder().add("msg","Message2").add("ip","10.10.10.11").build();
        logEntryRepository.add(e1);
        logEntryRepository.add(e2);

        Assert.assertEquals(3,logEntryRepository.fetch(start,  System.currentTimeMillis()).size());
        Assert.assertEquals(2,logEntryRepository.fetch(after0, System.currentTimeMillis()).size());
    }

    /*
     * This is executed when building the test plugin.
     * By dumping the environment variables to a property-file, we can make that data available
     * during the test
     */
    @Deployment
    public static Archive<? extends Assignable> deployTests() {
        Properties envProperties = new Properties();
        envProperties.putAll(System.getenv());
        try(FileWriter wr = new FileWriter("target/test-classes/env.properties",false)) {
            envProperties.store(wr,null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return ShrinkWrap.create(JavaArchive.class, "tests.jar")
                .addAsResource("env.properties");
    }
}
