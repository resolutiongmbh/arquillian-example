package de.resolution.arquillianexample;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class LogEntry {

    private final long timestamp;
    private final Map<String,String> attributes;

    private LogEntry(long timestamp, Map<String,String> attributes) {
        this.timestamp = timestamp;
        this.attributes = new HashMap<>(attributes);
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public Map<String,String> getAttributes() {
        return Collections.unmodifiableMap(this.attributes);
    }

    public static Builder builder() {
        return new Builder(System.currentTimeMillis());
    }

    public static Builder builder(long timestamp) {
        return new Builder(timestamp);
    }

    public static class Builder {
        private final long timestamp;
        private final Map<String,String> attributes = new HashMap<>();

        private Builder(long timestamp) {
            this.timestamp = timestamp;
        }

        public Builder add(String key, String value) {
            this.attributes.put(key,value);
            return this;
        }

        public LogEntry build() {
            return new LogEntry(this.timestamp, this.attributes);
        }
    }

}
