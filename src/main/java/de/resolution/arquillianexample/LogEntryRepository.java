package de.resolution.arquillianexample;

import java.util.List;

public interface LogEntryRepository {

    List<LogEntry> fetch(long from, long until);

    void add(LogEntry logEntry);
}
