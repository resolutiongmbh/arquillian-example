package de.resolution.arquillianexample.ao;

import net.java.ao.Entity;
import net.java.ao.OneToMany;
import net.java.ao.schema.Table;

@Table("LOGENTRY")
public interface AoLogEntry extends Entity {

    long getCreated();
    void setCreated(long timestamp);

    @OneToMany
    AoLogEntryAttribute[] getAoLogEntryAttributes();

}