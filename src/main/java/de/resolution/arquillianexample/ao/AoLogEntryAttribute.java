package de.resolution.arquillianexample.ao;

import net.java.ao.Entity;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

@Table("LOGENTRYATTRIBUTE")
public interface AoLogEntryAttribute extends Entity {

    @StringLength(StringLength.UNLIMITED)
    String getKey();
    void setKey(String key);

    @StringLength(StringLength.UNLIMITED)
    String getValue();
    void setValue(String value);

    AoLogEntry getAoLogEntry();
    void setAoLogEntry(AoLogEntry aoLogEntry);

}