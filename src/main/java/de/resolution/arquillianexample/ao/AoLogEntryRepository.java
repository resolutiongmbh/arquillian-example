package de.resolution.arquillianexample.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import de.resolution.arquillianexample.LogEntry;
import de.resolution.arquillianexample.LogEntryRepository;
import net.java.ao.Query;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a simple repository storing stuff in an AO-table.
 * Each log entry has a timestamp, and entries can be filtered by this value
 */
@ExportAsService
@Named("logEntryRepository")
public class AoLogEntryRepository implements LogEntryRepository {

    private final ActiveObjects ao;

    @Inject
    public AoLogEntryRepository(@ComponentImport ActiveObjects ao) {
        this.ao = ao;
    }

    @Override
    public List<LogEntry> fetch(long from, long until) {

        return ao.executeInTransaction(() -> {
            AoLogEntry[] aoLogEntries = ao.find(AoLogEntry.class,
                    Query.select().where("CREATED >= ? AND CREATED <= ?", from, until));

            List<LogEntry> logEntryList = new ArrayList<>();
            for (AoLogEntry aoLogEntry : aoLogEntries) {
                LogEntry.Builder builder = LogEntry.builder(aoLogEntry.getCreated());
                for (AoLogEntryAttribute aoLogEntryAttribute : aoLogEntry.getAoLogEntryAttributes()) {
                    builder.add(aoLogEntryAttribute.getKey(), aoLogEntryAttribute.getValue());
                }
                logEntryList.add(builder.build());
            }
            return logEntryList;
        });
    }

    @Override
    public void add(LogEntry logEntry) {
        ao.executeInTransaction(() -> {
            AoLogEntry aoLogEntry = ao.create(AoLogEntry.class);
            aoLogEntry.setCreated(logEntry.getTimestamp());
            aoLogEntry.save();
            logEntry.getAttributes().forEach((key,value) -> {
                AoLogEntryAttribute aoLogEntryAttribute = ao.create(AoLogEntryAttribute.class);
                aoLogEntryAttribute.setKey(key);
                aoLogEntryAttribute.setValue(value);
                aoLogEntryAttribute.setAoLogEntry(aoLogEntry);
                aoLogEntryAttribute.save();
            });
            return null;
        });
    }
}
